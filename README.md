# Postfix Mail Relay

Simple SMTP relay, cloned from [shamil/docker-postfix-relay](https://github.com/shamil/docker-postfix-relay)

## Description

The container provides a simple SMTP relay for environments like Amazon VPC where you may have private servers with no Internet connection
and therefore with no access to external mail relays (e.g. Amazon SES, SendGrid and others). You need to supply the container with your
external mail relay address and credentials. The image is tested with `Amazon SES`, `Sendgrid`, `Gmail` and `Mandrill`

## Changes from `shamil/docker-postfix-relay`

* Uses european Amazon SES SMTP server `email-smtp.eu-west-1.amazonaws.com` instead of US default `email-smtp.us-east-1.amazonaws.com`.
* Build instructions added to README
* SystemD instructions added to README
* more to come hopefully :D

## Environment variables

| ENV. Variable            | Description                                                                                                                      |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------|
| `ACCEPTED_NETWORKS`      | Space delimited list of networks to accept mail from. <br/> Default: `192.168.0.0/16 172.16.0.0/12 10.0.0.0/8`                   |
| `RECIPIENT_RESTRICTIONS` | Space delimited list of allowed `RCPT TO` addresses. <br/> Default: **unrestricted**                                             |
| `SMTP_HOST`              | External relay DNS name. <br/> Default: `email-smtp.eu-west-1.amazonaws.com`                                                     |
| `SMTP_PORT`              | External relay TCP port. <br/> Default: `25`                                                                                     |
| `SMTP_LOGIN`             | Login to connect to the external relay. <br/> **Required**                                                                       |
| `SMTP_PASSWORD`          | Password to connect to the external relay. <br/> **Required**                                                                    |
| `USE_TLS`                | Remote require tls. Must be `yes` or `no`. <br/> Default: `no`                                                                   |
| `TLS_VERIFY`             | Trust level for checking remote side cert. <br/> Default: `may` (http://www.postfix.org/postconf.5.html#smtp_tls_security_level) |

## Exposed port(s)

Postfix on port `25`

## How to use (Ubuntu 18.04)

Build the image

```bash
git clone git@gitlab.com:dieffe/docker---postfix-smtp-relay-using-alpine.git
 
cd docker---postfix-smtp-relay-using-alpine
 
sudo docker build -t smtp-relay .

```


Launch the container

```bash
sudo docker run -d --name="smtprelay" \
    -e SMTP_LOGIN=<Amazon SES smtp username> \
    -e SMTP_PASSWORD=<Amazon SES smtp password> \
    -p 25:25 \
    smtp-relay
```

Create SystemD service

```bash
sudo nano /etc/systemd/system/docker-smtprelay.service
```

```
[Unit]
Description=SMTP Relay Container
After=docker.service
Requires=docker.service
 
[Service]
Restart=always
ExecStart=/usr/bin/docker start -a smtprelay
ExecStop=/usr/bin/docker stop -t 2 smtprelay
 
[Install]
WantedBy=multi-user.target
```

Note:
*Restart=always* will make sure the service is always running. This means you cannot stop the container using docker commands.

```bash
sudo systemctl daemon-reload
sudo systemctl start docker-smtprelay
sudo systemctl enable docker-smtprelay
```

The smtprelay container will now start automatically with your Ubuntu server.

To check if it's running
```bash
sudo docker ps
```

To check the container performances
```bash
sudo docker stats
```

To check container logs
```bash
sudo docker logs -f smtprelay
```

To access container shell
```bash
sudo docker exec -ti smtprelay /bin/sh
```

To stop the container
```bash
sudo systemctl stop docker-smtprelay
```

To start the container
```bash
sudo systemctl start docker-smtprelay
```

To disable the SystemD Service
```bash
sudo systemctl disable docker-smtprelay
```


